import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {
  productObservable;

 //getProducts(){
 //     this.productObservable = this.af.database.list('/product');
  //  return this.productObservable;   
 //}

  //getProducts(){
    //return this.posts;
    //return this._http.get(this._url).map(res => res.json()).delay(2000);
    //  this.productObservable = this.af.database.list('/product').map(
     // products =>{
     //   products.map(
      //    product => {
      //      product.categoryNames = [];
       //     for(var p in product.category){
       //         product.categoryNames.push(
        //        this.af.database.object('/category/' + p)
        //      )
        //    }
        //  }
     //   );
     //   return products;
    //  }
  //  )
 //  return this.productObservable; 
//  }

getProducts(){
    this.productObservable = this.af.database.list('/product').map(
      products =>{
      {products.map(
         product => {
            product.categoryName = [];
                product.categoryName.push(
                this.af.database.object('/category/' + product.categoryId));
          });
        return products;
      }}
    );
   return this.productObservable; 
   }

deleteProduct(product){
     let productKey = product.$key;
    this.af.database.object('/product/' + productKey).remove();
  }
 
  constructor(private af:AngularFire) { }

}
